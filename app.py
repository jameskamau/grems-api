from flask import Flask,request,jsonify
from flask_restful import reqparse, abort, Api, Resource
from dicttoxml import dicttoxml
import json
import requests

app = Flask(__name__)
api = Api(app)
headers = {'Content-Type': 'application/xml'}




class placeBetTransaction(Resource):


    def get(self,trans_id):
        pass


    def post(self):

        args=request.get_json()

        OprtTrxId=args['OprtTrxId']
        MnoTrxId=args['MnoTrxId']
        MnoName=args['MnoName']
        BetAmt=args['BetAmt']
        BetDesc=args['BetDesc']
        ExpWonAmt=args['ExpWonAmt']
        Ccy=args['Ccy']
        TrxDtTm=args['TrxDtTm']
        PlyrCellNum=args['PlyrCellNum']
        TktNum=args['TktNum']
        Odds=args['Odds']
        ExpBonus=args['ExpBonus']
        GameId=args['GameId']
        ShopId=args['ShopId']
        Jackpot=args['Jackpot']
        OfficeType=args['OfficeType']
        Status=args['Status']


        json_obj = {"OprtTrxId" : OprtTrxId, "MnoTrxId" : MnoTrxId, "MnoName":MnoName, "BetAmt":BetAmt,"BetDesc":BetDesc,"ExpWonAmt":ExpWonAmt,"Ccy":Ccy,"TrxDtTm":TrxDtTm,"PlyrCellNum":PlyrCellNum,"TktNum":TktNum,"Odds":Odds,"ExpBonus":ExpBonus,"GameId":GameId,"ShopId":ShopId,"Jackpot":Jackpot,"OfficeType" : OfficeType,"Status":Status}

        xml = dicttoxml(json_obj,attr_type=False, custom_root="TrxInfo")
        response= requests.post('http://196.192.79.29/api/transactions/qrequest', data=xml, headers=headers).text
        print(xml)


        return response
        # return jsonify(OprtTrxId=OprtTrxId, MnoTrxId=MnoTrxId, MnoName=MnoName, BetAmt=BetAmt,BetDesc=BetDesc,ExpWonAmt=ExpWonAmt,
        #  Ccy=Ccy,TrxDtTm=TrxDtTm,PlyrCellNum=PlyrCellNum,TktNum=TktNum,Odds=Odds,ExpBonus=ExpBonus,GameId=GameId,ShopId=ShopId,
        #  Jackpot=Jackpot,OfficeType=OfficeType,Status=Status)




class dailyBalanceList(Resource):



    def get(self,trans_id):
        pass

    def post(self):
        args=request.get_json()
        trans_id = int(max(dailyTransaction.keys())) + 1
        dailyTransaction[trans_id]=args
        xml = dicttoxml(dailyTransaction,attr_type=False, custom_root="TrxInfo")
        response= requests.post('http://196.192.79.29/api/transactions/qrequest', data=xml, headers=headers).text
        print(xml)
        return dailyTransaction






class accountBalance(Resource):
    def get(self):
        pass

    def post(self):

        args=request.get_json()

        for key in args['key']:
            OprtTrxId=args[key]['OprtTrxId']
            PlyrId=args[key]['PlyrId']
            BlncAmt=args[key]['BlncAmt']
            Ccy=args[key]['Ccy']
            TrxDtTm=args[key]['TrxDtTm']

            json_obj = {"OprtTrxId" : OprtTrxId, "PlyrId" : PlyrId, "BlncAmt":BlncAmt, "Ccy":Ccy,"TrxDtTm":TrxDtTm}
        xml = dicttoxml(json_obj,attr_type=False, custom_root="TrxInfo")

        # response= requests.post('http://196.192.79.29/api/transactions/qrequest', data=xml, headers=headers).text

        print(xml)


        return jsonify(OprtTrxId=OprtTrxId, PlyrId=PlyrId, BlncAmt=BlncAmt, Ccy=Ccy,TrxDtTm=TrxDtTm)




class betOutcome(Resource):
    def get(self):
        pass

    def post(self):
        args=request.get_json()

        OprtTrxId=args['OprtTrxId']
        OrgnTktNum=args['OrgnTktNum']
        WonAmt=args['WonAmt']
        Bonus=args['Bonus']
        TrxDtTm=args['TrxDtTm']
        Status=args['Status']


        json_obj = {"OprtTrxId" : OprtTrxId, "OrgnTktNum" : OrgnTktNum, "WonAmt":WonAmt, "Bonus":Bonus,"TrxDtTm":TrxDtTm,"Status":Status}
        xml = dicttoxml(json_obj,attr_type=False, custom_root="TrxInfo")

        response= requests.post('http://196.192.79.29/api/transactions/qrequest', data=xml, headers=headers).text

        print(xml)

        return response
        # return jsonify(OprtTrxId=OprtTrxId, OrgnTktNum = OrgnTktNum, WonAmt=WonAmt, Bonus=Bonus,TrxDtTm=TrxDtTm,Status=Status)




api.add_resource(placeBetTransaction,'/api/v1/placeBetTransaction')
api.add_resource(dailyBalanceList,'/api/v1/dailyBalanceList')
api.add_resource(accountBalance,'/api/v1/accountBalance')
api.add_resource(betOutcome,'/api/v1/betOutcome')


if __name__ == '__main__':
    app.run(debug=True)
    # app.run(debug=True,host="0.0.0.0",port="80")
